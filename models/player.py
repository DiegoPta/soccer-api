"""
Defines the data model that represents a soccer player in the database.
"""

# SQLAlchemy imports.
from sqlalchemy import Column, Integer, String, Date

# Project imports.
from database.config import BASE


class PlayerDB(BASE):

    __tablename__ = "players"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    country = Column(String)
    position = Column(String)
    good_leg = Column(String)
    birthdate = Column(Date)