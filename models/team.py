"""
Defines the data model that represents a soccer team in the database.
"""

# SQLAlchemy imports.
from sqlalchemy import Column, Integer, String

# Project imports.
from database.config import BASE


class TeamDB(BASE):

    __tablename__ = "teams"

    id = Column(Integer, primary_key=True, autoincrement=True)
    team_id = Column(Integer)
    name = Column(String)
    abbreviation = Column(String)
    country = Column(String)
    manager = Column(String)
    stars = Column(Integer)
