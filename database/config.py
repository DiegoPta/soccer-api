"""
Database configuration script.
"""

# Python imports.
import os

# SQL Alchemy imports.
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sqlite_file_name = "../database.sqlite"
base_dir = os.path.dirname(os.path.realpath(__file__))

database_url = f"sqlite:///{os.path.join(base_dir, sqlite_file_name)}"

ENGINE = create_engine(database_url, echo=True)

SESSION = sessionmaker(bind=ENGINE)

BASE = declarative_base()