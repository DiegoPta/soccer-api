# Soccer Api
API of soccer teams and players.

## Description
API for management of soccer teams and players. Implemented in Python using the FastAPI framework.

***

## Installation
- Clone the project from the repository.
- Creates a virtual environment to the project. Can use the following command: python -m venv venv.
- Install required dependeces to the project. Use the following command: pip install -r requirements.py.
- Run the app with uvicorn like this: uvicorn main:app.

## Usage
After running the application, you can access the API documentation through the browser with the following path: localhost:8000/docs.