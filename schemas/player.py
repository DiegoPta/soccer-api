"""
This scripts defines a model that represents a soccer player.
"""

# Python imports.
from datetime import date
from typing import Optional

# Pydantic imports.
from pydantic import BaseModel
from pydantic import Field


class PlayerBase(BaseModel):
    name: str = Field(..., min_length=1, max_length=50)
    country: str = Field(..., max_length=50)
    position: str = Field(..., max_length=20)
    good_leg: str = Field(..., min_length=4, max_length=5)
    birthdate: date = Field(...)


class PlayerCreate(PlayerBase):
    team_id: int = Field(...)


class Player(PlayerBase):
    team: str

    class Config:
        orm_mode = True
