"""
This scripts defines a model that represents a soccer team.
"""

# Python imports.
from typing import Optional

# Pydantic imports.
from pydantic import BaseModel
from pydantic import Field


class TeamBase(BaseModel):
    name: str = Field(..., min_length=1, max_length=50)
    abbreviation: str = Field(..., min_length=1, max_length=4)
    country: str = Field(..., max_length=50)
    manager: str = Field(..., min_length=5, max_length=50)
    stars: Optional[int] = Field(default=0)


class Team(TeamBase):
    id: int

    class Config:
        orm_mode = True