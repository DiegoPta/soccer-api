"""
This script executes the application.
"""

# Python imports.

# Pydantic imports.

# FastAPI imports.
from fastapi import FastAPI
from fastapi import HTTPException
from fastapi.responses import HTMLResponse, JSONResponse

# Project imports.
from database.config import SESSION, ENGINE, BASE
from models.team import TeamDB


app = FastAPI()


BASE.metadata.create_all(bind=ENGINE)


@app.get("/",
         tags=["Home"],
         summary="Get home page",
         status_code=200)
async def home() -> HTMLResponse:
    """
    Home path operation.
    - **return**: Html with a reference to the documentation.
    """
    home_string = """
    <h1>Soccer API</h1>
    <h4>Welcome to the Soccer API home page!</h4>
    <p><a href="http://localhost:8000/docs">Go to documentation</a></p>
    """
    return HTMLResponse(home_string)
